#include "LedStrip.h"
#include <algorithm>    // std::min/max

/** LED STRIP CONFIGURATION **/
const int DATA_PIN = 0; // 2;
#ifndef STRIP_TYPE // allow changing strip by build flag
    #define STRIP_TYPE NEOPIXEL
#endif
/** END CONFIGURATION **/

LedStrip::LedStrip(int led_count) {
    this->led_count = led_count;
    this->leds = new CRGB[led_count];
    this->clear();
    this->setBrightness(80);

    FastLED.addLeds<STRIP_TYPE, DATA_PIN>(this->leds, this->led_count);
}
LedStrip::~LedStrip() {
    delete[] leds;
}

int LedStrip::length() {
    return this->led_count;
}

void LedStrip::clear() {
    for(int i = 0; i < this->led_count; i++) {
        this->leds[i] = CRGB(0,0,0);
    }
    this->show();
}

void LedStrip::show() {
    FastLED.show();
}

int LedStrip::getBrightness() {
    return this->brightness;
}
//range 1-100
void LedStrip::setBrightness(int percentage) {
    // just return if bigger then 100
    if (percentage > 100 || percentage < 0) return;

    //ONLY FUNCTION ON 25% OF CAPACITY
    const uint8_t maxVal = 64;

    this->brightness = percentage;
    double interpolate = (std::max(0,std::min(percentage,100)) / 100.0) * maxVal;
    FastLED.setBrightness(interpolate);
    show();
}

/**
 * @param uint8_t r 0-255
 * @param uint8_t g 0-255
 * @param uint8_t b 0-255
 */
void LedStrip::setPixelColor(int i, uint8_t r, uint8_t g, uint8_t b)
{
    this->leds[i] = CRGB(r,g,b);
};
void LedStrip::setPixelColor(int i, CRGB rgb) {
    this->leds[i] = CRGB(rgb);
}
void LedStrip::setPixelColor(int i, CHSV hsv) {
    this->leds[i] = CRGB(hsv);
}

CRGB LedStrip::getPixel(int i) {
    return this->leds[i];
}

CRGB *LedStrip::getLeds(){
    return leds;
}
