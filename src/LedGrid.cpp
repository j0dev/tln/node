#include "LedGrid.h"

LedGrid::LedGrid(int led_count, uint8_t width) : LedStrip(led_count) {
    this->grid_width = width;
    this->grid_height = led_count / width;
}

uint8_t LedGrid::width() {
    return this->grid_width;
}
uint8_t LedGrid::height() {
    return this->grid_height;
}

void LedGrid::setGridPixelColor(uint8_t x, uint8_t y, uint8_t r, uint8_t g, uint8_t b) {
    // calculate led offset
    size_t i = x + (y * this->grid_width);
    // set led
    leds[i] = CRGB(r,g,b);
}
void LedGrid::setGridPixelColor(uint8_t x, uint8_t y, CRGB rgb) {
    // calculate led offset
    size_t i = x + (y * this->grid_width);
    // set led
    leds[i] = CRGB(rgb);
}
void LedGrid::setGridPixelColor(uint8_t x, uint8_t y, CHSV hsv) {
    // calculate led offset
    size_t i = x + (y * this->grid_width);
    // set led
    leds[i] = CRGB(hsv);
}

CRGB LedGrid::getPixel(uint8_t x, uint8_t y) {
    // calculate led offset
    size_t i = x + (y * this->grid_width);
    // return led
    return leds[i];
}
