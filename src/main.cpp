/** Includes **/
#include <Arduino.h>
#include <NetworkManager.h>
#include <ESPAsyncWebServer.h>

#include "LedGrid.h"

/** Project definition **/
#define PRJ_NAME "TLN-Node"
#define PRJ_VER  "0.0.1"

#ifndef WEB_PORT
  #define WEB_PORT 80
#endif

#ifndef LED_COUNT
  #define LED_COUNT 300
#endif
#ifndef LED_ROW_COUNT
  #define LED_ROW_COUNT 14
#endif

/** Global variables **/
AsyncWebServer server(WEB_PORT);
LedGrid ledgrid(LED_COUNT, LED_ROW_COUNT);

/**
 * setupNetwork will initiate the network
 */
void setupNetwork() {
  // overrideing default networkconfig
  networkManager.setHostnamePrefix("TLN-");
  networkManager.setApPSK("TimesLightNetwork");
  // initialize all modules and config
  networkManager.begin();
  // start the networking
  networkManager.start();

  // webserver setup
  server.begin();
  networkManager.webInit(&server, WEB_PORT, true); // webroot will be true until we implement our own

  Serial.printf("Hostname: %s\n", networkManager.getHostname());
  // AP Network serial output
  if (networkManager.isApEnabled()) {
    Serial.println("AP active:");
    if (networkManager.isApTimeoutActive()) {
      Serial.printf(" AP will disable in %ds\n", networkManager.getApTimeout());
    }
    Serial.printf(" SSID: %s\n", networkManager.getApSSID().c_str());
    Serial.print(" IP Address: ");
    if (networkManager.isApStaticIpEnabled()) {
      Serial.println(networkManager.getApIp());
    } else {
      Serial.println(WiFi.softAPIP());
    }
  }

  // sleep until our network is properly up, needed for esp8266 mDNS
  if (networkManager.isStaEnabled()) {
    if (WiFi.waitForConnectResult() != WL_CONNECTED) {
      Serial.printf("WiFi did not connect in time!\n");
      return;
    }
  }

  // WiFi Network serial output
  if (networkManager.isStaEnabled()) {
    Serial.println("WiFi connected:");
    Serial.printf(" SSID: %s\n", networkManager.getStaSSID().c_str());
    Serial.print(" IP Address: ");
    Serial.println(WiFi.localIP());
  }
}

void setup() {
  // Serial start
  Serial.begin(115200);
  Serial.println("TimesLightNetwork Node starting up..");

  // reset leds and set brightness (resetting because it may not properly set at pre-setup)
  ledgrid.clear();
  ledgrid.setBrightness(ledgrid.getBrightness());

  delay(5);

  // boot status led update
  ledgrid.setPixelColor(1, CHSV(145, 230, 255));
  ledgrid.show();

  // network start
  Serial.println("Starting network..");
  setupNetwork();

  // boot status led update
  ledgrid.setPixelColor(2, CHSV(135, 230, 255));
  ledgrid.show();
}

void loop() {
  networkManager.loop();

  delay(10);
}