#pragma once

#define FASTLED_ALLOW_INTERRUPTS 0
#define FASTLED_INTERRUPT_RETRY_COUNT 0

#include <FastLED.h>

class LedStrip {
    public:
        LedStrip (int led_count);
        ~LedStrip();

        int length();
        void clear();
        void show();

        int getBrightness();
        void setBrightness(int percentage);

        void setPixelColor(int i, uint8_t r, uint8_t g, uint8_t b);
        void setPixelColor(int i, CRGB rgb);
        void setPixelColor(int i, CHSV hsv);
        CRGB getPixel(int i);

        CRGB *getLeds();

    protected:
        int led_count;
        CRGB *leds;
        int brightness;
};
