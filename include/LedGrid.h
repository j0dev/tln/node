#pragma once

#include "LedStrip.h"

class LedGrid : public LedStrip {
    public:
        LedGrid(int led_count, uint8_t width);
        // ~LedGrid();

        uint8_t width();
        uint8_t height();

        void setGridPixelColor(uint8_t x, uint8_t y, uint8_t r, uint8_t g, uint8_t b);
        void setGridPixelColor(uint8_t x, uint8_t y, CRGB rgb);
        void setGridPixelColor(uint8_t x, uint8_t y, CHSV hsv);

        CRGB getPixel(uint8_t x, uint8_t y);

    protected:
        uint8_t grid_width;
        uint8_t grid_height;
};
